#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <netdb.h>
#include <strings.h>
#include "string.h"
#include <sys/ioctl.h>


#define FTP_PORT 21
#define FTP_QUIT "QUIT\n"
#define FTP_PASV "PASV\n"
#define FTP_HELP "HELP\n"
#define FTP_LIST "LIST\n"
#define FTP_USER "USER "
#define FTP_PASS "PASS "
#define FTP_RETR "RETR "
#define DEBUG 0


void getAddress(char * hostName, char * hostIP);
int sendCommand(int sockfd, char * message);
int getResponse(int sockfd, char * message);
void clearScreen();
void parsePasv(char * pasv,char * ip, int * port);
int connectToServer(char * ip, int port);
int getFile(int sockfd, char * fileName);
int parseUserAddr(char * addr);
void exit_error(char * message);
void getFileName(char * filename);


typedef struct {
	char user[256];
	char pass[256];
	char host[256];
	char path[256];
	int hasUser;
	int hasPassword;
} credentials;

credentials data;




int main(int argc, char** argv) {

	clearScreen();
	
	if(argc != 2) {
		printf("Usage error\nUsage example: ./ftp ftp://user:password@host/path_to_file\n");
		return 0;
	}
	
	int	sockfd;
	char	buf[256];
	char	response[1024]; 
	char	serverIP[17];
	char	user[256];
	char	pass[256];
	char	retr[256];
	char	filename[256];
	char * host = argv[1];
	char  transferIP[32];
	int transferPort;
	int sockfdtransfer;
	int responseCode;
	
	parseUserAddr(host);
	getAddress(data.host, serverIP);
	sockfd = connectToServer(serverIP, FTP_PORT);

	// accepting connection
	responseCode = getResponse(sockfd, response);
	if (responseCode != 220) exit_error("Connection to host failed!\n");
	printf("Connected!\n");
	
	// sending user
	strcpy(user, FTP_USER);
	strcat(user, data.user);
	strcat(user,"\n");
	sendCommand(sockfd, user);
	responseCode = getResponse(sockfd, response);

	// sending password
	strcpy(pass, FTP_PASS);
	strcat(pass, data.pass);
	strcat(pass, "\n");
	sendCommand(sockfd, pass);
	responseCode =  getResponse(sockfd, response);
	if (responseCode != 230) exit_error("Incorrect login!\n");

	// sending pasv
	strcpy(buf, FTP_PASV);
	sendCommand(sockfd, buf);
	responseCode = getResponse(sockfd, response);
	if (responseCode != 227) exit_error("Pasv refused!\n");

	// parsing pasv response
	parsePasv(response, transferIP, &transferPort);
	if (DEBUG) printf("%s\n", transferIP);
	if (DEBUG) printf("%d\n", transferPort);
	sockfdtransfer	= connectToServer(transferIP, transferPort);
	if (sockfdtransfer < 0) exit_error("Failed to initiate transfer!\n");

	// sendind retr
	strcpy(retr, FTP_RETR);
	strcat(retr, data.path);
	strcat(retr, "\n");
	sendCommand(sockfd, retr);
	responseCode = getResponse(sockfd, response);
	if (responseCode != 150) exit_error("File not found!");
	
	// transfering file
	getFileName(filename);
	getFile(sockfdtransfer,filename);

	// quitting
	sendCommand(sockfdtransfer, "quit\n");
	sendCommand(sockfd, "quit\n");

	// closing sockets
	printf("File transfer complete!\n");
	close(sockfdtransfer);
	close(sockfd);

	exit(0);
}




void getAddress(char * hostName, char * hostIP)
{
	struct hostent * h;
	
	if ((h=gethostbyname(hostName)) == NULL) {  
		herror("gethostbyname");
		exit(1);
	}

	char * addr = inet_ntoa(*((struct in_addr *)h->h_addr));

	printf("Host name  : %s\n", h->h_name);
	printf("IP Address : %s\n", addr);

	strcpy(hostIP, addr);
}




int connectToServer(char * ip, int port)
{
	int sockfd;
	struct	sockaddr_in server_addr;

	/*server address handling*/
	bzero((char*)&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(ip);	/*32 bit Internet address network byte ordered*/
	server_addr.sin_port = htons(port);		/*server TCP port must be network byte ordered */

	/*open an TCP socket*/
	if ((sockfd = socket(AF_INET,SOCK_STREAM,0)) < 0) {
		perror("socket()");
		exit(0);
	}

	/*connect to the server*/
	if (connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
		perror("connect()");
		exit(0);
	}

	return sockfd;
}




int sendCommand(int sockfd, char * message)
{
	int bytes = write(sockfd, message, strlen(message));
	
	if (DEBUG) printf("Bytes: %d; ", bytes);
	if (DEBUG) printf("Message: %s", message);

	return bytes;
}




int getResponse(int sockfd, char * message)
{
	int code_found = 0;
	int response_is_complete=0;
	int index =0;
	int number_seq=0;
	int new_line;
	char code[4];
	int bytes;
	char * pch;
	int len = 0;
	ioctl(sockfd, FIONREAD, &len);

	/*
	 * O servidor pode enviar multiplas respostas a um comando.
	 * A forma de saber que vai haver mais que uma resposta é
	 * quando imediatamente a seguir ao response_code estiver um -
	 * A resposta final sera encontrada quando se voltar a encontrar o response_code seguido de um caracter de espaco
	 * */
	while(!response_is_complete){
		bytes = read(sockfd, message, 1024);
		
		if(bytes<=0){
			perror("Unable to get response from host\n");
			exit(1);
		}
		index=0;

		while(index < 3 && !code_found){
			if(isdigit(message[index])){
				code[index]= message[index];
				if(index==2){
					code_found = 1;
					index++;
					if(message[index] !='-'){ //se for "-" o servidor ainda manda mais respostas
						response_is_complete =1;
						code[3]='\0';
					}
				}
			}
			index++;
		}

		while(!response_is_complete && message[index] != '\0'){

			if(message[index] == '\n'){
				/*
				 * quando encontrar o \n pode ser uma nova linha com o response_code seguido de espaço,
				 * o que significa que é a última mensagem
				 * */
				new_line=1;
				index++;
				number_seq=0;
			}

			if(new_line && message[index] == code[number_seq] && number_seq<4){
				number_seq++;
				if(number_seq == 3){
					index++;
					char to_debug = message[index];
					if(message[index] != '-')
						response_is_complete = 1;
				}
			}
			else{
				new_line=0;
				number_seq=0;
			}

			index++;
		}
		//Se ate aqui nao esta pronto ainda para sair, ler do buffer novamente...
	}

	message[bytes] = '\0';
	code[4] = '\0';
	if (DEBUG) printf("Bytes: %d\n ",bytes);
	if (DEBUG) printf("Response: %s\n", message);

	if (DEBUG) printf("%s\n",code);
	return atoi(code);
}




void parsePasv(char * pasv, char * ip, int * port) 
{
	char * pch;
	char ipBuilder[32], portPart1[32], portPart2[32];
	int i = 0;
	
	strcpy(ipBuilder,"");
	pch = strtok (pasv,"(");
	
	for (i = 0; i < 6; i++)
	{
		pch = strtok (NULL, "(,)");		
		if ( i < 4) 
		{
			strcat(ipBuilder,pch);
			if (i < 3) strcat(ipBuilder, ".");
		}
		if (i == 4) strcpy(portPart1, pch);
		if (i == 5) strcpy(portPart2, pch);
	}
	
	(* port) = atoi(portPart1) * 256 + atoi(portPart2);
	strcpy(ip, ipBuilder);
}




int parseUserAddr(char * addr)
{
	char protocol[6];
	memcpy(protocol, addr, 6);
	
	if (strcmp(protocol, "ftp://") != 0)  exit(1);
	
	int index = 6;
	char buf[256], user[256], pass[256], host[256];
	int offset = 6;
	int goOn = 1;
	int foundColon = 0;
	data.hasUser = 0;
	data.hasPassword = 0;
	int foundHost = 0;
	
	while(goOn){

		switch(addr[index]){
		
		case '\0':
			if (data.hasUser == 0) strcpy(user, "anonymous");
			goOn = 0;
			break;
			
		case '/':
			if (foundHost == 0) {
				strcpy(host,buf);
				host[index - offset] = '\0';
				strcpy(buf,"");
				offset = index ;
				foundHost = 1;
			}
			break;

		case '@':
			if(foundColon){
				strcpy(pass,buf); 
				pass[index - offset] = '\0';
				offset = index + 1;
				strcpy(buf,"");
				data.hasPassword = 1;
			}
			else {
				data.hasUser = 1;
				strcpy(user,buf); 
				user[index - offset] = '\0';
				offset = index + 1;
				strcpy(buf,"");
			}
			foundColon = 0;
			break;
			
		case ':':
			data.hasUser = 1;
			strcpy(user,buf); 
			user[index - 6] = '\0';
			offset = index + 1;
			strcpy(buf,"");
			foundColon = 1;
			break;
		}
		
		buf[index - offset] = addr[index];
		index++;
	}

	if (foundColon || strcmp(host, "") == 0 || strcmp(buf, "") == 0) exit(1);
	
	strcpy(data.host,host);
	strcpy(data.user,user);
	strcpy(data.pass,pass);
	strcpy(data.path,buf);
	if (!data.hasUser) strcpy(data.pass, "anon@fe.up.pt");

	if (DEBUG) {
		printf("%s\n",data.user);
		printf("%s\n",data.pass);
		printf("%s\n",data.host);
		printf("%s\n",data.path);
	}
	return 0;
}




void getFileName(char *filename)
{
	char buf[256];
	int goOn = 1;
	int index = 0;
	int bufIndex = 0;

	while(goOn) {

		if (data.path[index] == '\0') break;

		if (data.path[index] == '/') {
			bufIndex = -1;
		}
		else buf[bufIndex] = data.path[index];
		bufIndex++;
		index++;
	}
	
	buf[bufIndex] = '\0';
	strcpy(filename, buf);
}




int getFile(int sockfd, char * filename)
{
	int fdFile = open(filename, O_CREAT | O_WRONLY | O_TRUNC,0666);
	int bytes = 0;
	char buf[256];
	
	do {
		bytes = read(sockfd, buf, 255);
		write(fdFile, buf, bytes);
	} while(bytes != 0);

	close(fdFile);
	return bytes;
}




void exit_error(char * message)
{
	printf("error: %s\n",message);
	exit(1);
}




void clearScreen()
{
	if (fork() == 0) {
		execlp("clear","clear",NULL); 
		exit(0);
	}

	int status;
	wait(&status);
}